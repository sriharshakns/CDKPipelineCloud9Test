import { Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as cdk from 'aws-cdk-lib';
import { CodePipeline, CodePipelineSource, ShellStep } from 'aws-cdk-lib/pipelines';

export class CdkPipelineCloud9TestStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    // The code that defines your stack goes here

    // example resource
    const pipeline = new CodePipeline(this, 'Pipeline', {
      pipelineName: 'MyPipeline',
      synth: new ShellStep('Synth', {
        input: CodePipelineSource.gitHub('sriharshakns/CDKPipelineCloud9Test', 'main'),
        commands: ['npm ci', 'npm run build', 'npx cdk synth']
      })
    });

    const queue = new sqs.Queue(this, 'CdkPipelineCloud9TestQueue123', {
      visibilityTimeout: cdk.Duration.seconds(300)
    });
  }
}
